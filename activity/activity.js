// Gerson de Ocampo | BCS21
function oddEvenChecker(x) {
    switch (x % 2) {
        case 0:
            return console.log("The number is even");
        case 1:
            return console.log("The number is odd");
        default:
            return console.log("Invalid Input");
  }
}

oddEvenChecker(8);
oddEvenChecker(21);
oddEvenChecker("Gerson")

function budgetChecker(y) {
    switch (y > 40000) {
        case true:
            return console.log("You are over the budget.");
        case false:
            return console.log("You have resources left.");
        default:
            return console.log("Invalid Input");
    }
}

budgetChecker(28000);
budgetChecker(80000);
budgetChecker("Miyuki");
// Gerson de Ocampo | BCS21